# Installation #

Create a new virtual environment

```
#!bash

virtualenv library_env
```
Activate the virtualenv

```
#!bash

cd library_env/ && source bin/activate
```
Clone the library_api repo

```
#!bash

git clone git@bitbucket.org:hakanyolat/library_api.git
```
Run the following command to install the required dependencies with the pip package manager.

```
#!bash

cd library_api/ && pip install -r requirements.txt
```
Make migrations.

```
#!bash

python manage.py makemigrations library_api
```
And migrate them.

```
#!bash

python manage.py migrate
```
Now you can run the django.

```
#!bash

python manage.py runserver
```

# Endpoints #

* **(POST) /api/library/ ** Create a new library with a csv file.
* **(PATCH) /api/library/ ** Extend the existing library with a csv file.
* **(GET) /api/author/ ** Lists all the authors.
* **(GET) /api/book/ ** Lists all the books.
* **(POST) /api/author/ ** Creates a new author with name, surname and date_of_birth parameters.
* **(POST) /api/book/ ** Creates a new book with isbn, title, lc_classification and authors parameters.
* **(GET) /api/author/<id>/ ** Shows the information of an existing author.
* **(GET) /api/book/<id>/ ** Shows the information of an existing book.
* **(PUT) /api/author/<id>/ ** It updates an existing author with all parameters.
* **(PUT) /api/book/<id>/ ** It updates an existing book with all parameters.
* **(PATCH) /api/author/<id>/ ** An existing author is updated according to the given parameters.
* **(PATCH) /api/book/<id>/ ** An existing book is updated according to the given parameters.
* **(DELETE) /api/author/<id>/ ** Deletes an existing author.
* **(DELETE) /api/book/<id>/ ** Deletes an existing book.

# Client (library_client/library_client.py) #
This client file allows you to control the library API. The following commands apply to library_client.py.

```
#!bash

> upload library --file library.csv
> add library --file library.csv
> list author
> list book
> new author --name John --surname Doe --date_of_birth 01.01.1980
> new book --isbn ISBN009 --title Django --lc_classification CS --authors 1
> put author --id 1 --name Foo --surname Bar --date_of_birth 01.01.1974
> put book -id 1 --isbn ISBN012 --title Python --lc_classification CS --authors 4,12
> patch author --id 1 --name John
> patch book -id 1 --lc_classification AB
> delete author --id 1
> delete book --id 1
```