import argparse
import requests


class LibraryClient(object):
    def __init__(self):
        self.argl = {
            'command': {'help': '(list|upload|new|put|patch)'},
            'model': {'help': '(author|book)'},
            '--file': {
                'type': file,
                'nargs': '?',
                'help': '(csv for add,upload|json)'
            },
            '--id': {'help': 'Author or Book ID'},
            '--name': {'help': 'Author name'},
            '--surname': {'help': 'Author surname'},
            '--date_of_birth': {'help': 'Author date of birth'},
            '--isbn': {'help': 'Book ISBN code'},
            '--title': {'help': 'Book title'},
            '--lc_classification': {'help': 'LC classification'},
            '--authors': {'help': 'Author ids. e.g: 1|1,2,3'}
        }
        self.endpoints = {
            'upload.library': 'post::library/',
            'add.library': 'patch::library/',
            'list.author': 'get::author/',
            'list.book': 'get::book/',
            'new.author': 'post::author/',
            'new.book': 'post::book/',
            'put.author': 'put::author/%s/',
            'put.book': 'put::book/%s',
            'patch.author': 'patch::author/%s/',
            'patch.book': 'patch::book/%s/',
            'delete.author': 'delete::author/%s/',
            'delete.book': 'delete::book/%s/'
        }
        self.url = 'http://127.0.0.1:8000/api/'
        self.endpoint_method = None
        self.args = self.create_parser().parse_args()
        self.generate_cli_response()

    def generate_cli_response(self):
        response = self.create_http_request()
        if self.args.command == 'list':
            if len(response):
                if self.args.model == 'author':
                    print ''
                    print self.get_author_row_format().format(
                        'ID',
                        'Name',
                        'Surname',
                        'Date of Birth'
                    )
                    print self.get_author_row_format().format(
                        '-'*5, '-'*16, '-'*16, '-'*16
                    )
                    for author in response:
                        fields = response[author]
                        print self.get_author_row_format().format(
                            author,
                            fields['name'],
                            fields['surname'],
                            fields['date_of_birth']
                        )
                    print ''
                elif self.args.model == 'book':
                    print ''
                    print self.get_book_row_format().format(
                        'ID',
                        'Isbn',
                        'Title',
                        'LCC',
                        'Authors'
                    )
                    print self.get_book_row_format().format(
                        '-'*5, '-'*13, '-'*30, '-'*6, '-'*10
                    )
                    for book in response:
                        fields = response[book]
                        print self.get_book_row_format().format(
                            book,
                            fields['isbn'],
                            fields['title'],
                            fields['lc_classification'],
                            ','.join([
                                id for id in fields['authors']
                            ])
                        )
                    print ''

            else:
                print 'There is no record to show.'

        else:
            response_color = {
                'success': 'green',
                'error': 'red'
            }
            print self.colored_text(
                response_color[response['response']],
                response['message']
            )

    def get_book_row_format(self):
        return '{:5} {:13} {:30} {:6} {:10}'

    def get_author_row_format(self):
        return '{:5} {:16} {:16} {:16}'

    def colored_text(self, color, string):
        colors = {
            'red': '\033[91m',
            'green': '\033[92m',
        }
        end = '\033[0m'
        return '%s%s%s' % (colors[color], string, end)

    def create_url(self, path):
        return '%s%s' % (self.url, path)

    def create_dict_from_args(self, keys):
        return {
            key: getattr(self.args, key)
            for key in keys if getattr(self.args, key)
        }

    def prepare_request(self):
        endpoint_key = '.'.join([self.args.command, self.args.model])
        endpoint_map = self.endpoints.get(endpoint_key, None)
        if endpoint_map:
            command, model = self.args.command, self.args.model
            self.endpoint_method, endpoint_path = endpoint_map.split('::')
            author_args = ['name', 'surname', 'date_of_birth']
            book_args = ['isbn', 'title', 'lc_classification', 'authors']
            return {
                'kwargs': {
                    'url': self.create_url(endpoint_path),
                    'files': {
                        'file': self.args.file
                    } if self.args.file else False
                }
            } if command == 'upload' else {
                'kwargs': {
                    'url': self.create_url(endpoint_path),
                    'headers': {'Content-Type': 'application/octet-stream'},
                    'data': self.args.file
                }
            } if command == 'add' else {
                'kwargs': {
                    'url': self.create_url(endpoint_path)
                }
            } if command == 'list' else {
                'kwargs': {
                    'url': self.create_url(endpoint_path),
                    'data': self.create_dict_from_args(author_args)
                    if model == 'author' else
                    self.create_dict_from_args(book_args)
                }
            } if command == 'new' else {
                'kwargs': {
                    'url': self.create_url(endpoint_path % self.args.id),
                    'data': self.create_dict_from_args(author_args)
                    if model == 'author' else
                    self.create_dict_from_args(book_args)
                }
            } if command in ['put', 'patch'] else {
                'kwargs': {
                    'url': self.create_url(endpoint_path % self.args.id)
                }
            } if command == 'delete' else False
        else:
            return False

    def create_parser(self):
        parser = argparse.ArgumentParser()
        for arg, kwargs in self.argl.items():
            parser.add_argument(arg, **kwargs)
        return parser

    def create_http_request(self):
        request_kwargs = self.prepare_request()['kwargs']
        request = getattr(requests, self.endpoint_method)(**request_kwargs)
        return request.json()


if __name__ == '__main__':
    LibraryClient()
