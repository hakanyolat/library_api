from __future__ import unicode_literals
from django.db import models
import re


class Author(models.Model):
    name = models.CharField(max_length=16)
    surname = models.CharField(max_length=16, unique=True)
    date_of_birth = models.DateField()

    def __str__(self):
        return ' '.join([self.name, self.surname])

    @staticmethod
    def fields_is_valid(params=None):
        if params is not None:
            patterns = {
                'name': r'[a-zA-Z\s]{1,16}$',
                'surname': r'[a-zA-Z\s]{1,16}$',
                'date_of_birth': r'(\d{1,2}[/.-]\d{1,2}[/.-]\d{4})$'
            }
            for field, value in params.items():
                if field in patterns and not re.match(patterns[field], value):
                    return False
            return True
        return False

    @classmethod
    def to_dict(cls, multi=True, **kwargs):
        authors = cls.objects.all().filter(**kwargs)
        if authors:
            return {
                author.id: {
                    'name': author.name,
                    'surname': author.surname,
                    'date_of_birth': author.date_of_birth.strftime('%d.%m.%Y')
                }
                for author in authors
            } if len(authors) > 1 or multi else {
                'name': authors[0].name,
                'surname': authors[0].surname,
                'date_of_birth': authors[0].date_of_birth.strftime('%d.%m.%Y')
            }
        else:
            raise Author.DoesNotExist


class Book(models.Model):
    isbn = models.CharField(max_length=13, unique=True)
    title = models.CharField(max_length=60)
    authors = models.ManyToManyField(Author, 'authors')
    lc_classification = models.CharField(max_length=6)

    def __str__(self):
        return self.title

    @staticmethod
    def fields_is_valid(params=None):
        if params is not None:
            patterns = {
                'isbn': r'[a-zA-Z0-9\s\-]{1,13}$',
                'title': r'[a-zA-Z0-9\s]{1,60}$',
                'authors': r'^[0-9]+,[0-9]+|[0-9]+',
                'lc_classification': r'[a-zA-Z0-9]{1,6}$',
            }
            for field, value in params.items():
                if field in patterns and not re.match(patterns[field], value):
                    return False
            return True
        return False

    @classmethod
    def to_dict(cls, multi=True, **kwargs):
        books = cls.objects.all().filter(**kwargs)
        if books:
            return {
                book.id: {
                    'isbn': book.isbn,
                    'title': book.title,
                    'lc_classification': book.lc_classification,
                    'authors': Author.to_dict(pk__in=[
                        author.id for author in book.authors.all()
                        ])
                }
                for book in books
            } if len(books) > 1 or multi else {
                'isbn': books[0].isbn,
                'title': books[0].title,
                'lc_classification': books[0].lc_classification,
                'authors': Author.to_dict(pk__in=[
                    author.id for author in books[0].authors.all()
                    ])
            }
        else:
            raise Book.DoesNotExist
