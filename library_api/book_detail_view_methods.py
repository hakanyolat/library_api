from django.http import HttpResponse, Http404, QueryDict
from library_api.models import Author, Book
from library_http import LibraryResponse


def get(r, book_id):
    try:
        return LibraryResponse.custom_response(Book.to_dict(
            multi=False, pk=book_id
        ))
    except Book.DoesNotExist:
        return LibraryResponse.record_not_found()


def put(r, book_id):
    return patch(r, book_id)


def patch(r, book_id):
    query = QueryDict(r.body).copy()
    params = ['isbn', 'title', 'lc_classification', 'authors']
    params_map = [query.get(p, False) for p in params]
    requests_is_not_available = True in [
        False in params_map and r.method == 'PUT',
        all(i is False for i in params_map) and r.method == 'PATCH'
    ]
    if requests_is_not_available:
        return LibraryResponse.invalid_request()
    elif not Book.fields_is_valid(query):
        return LibraryResponse.invalid_request_parameters()
    else:
        try:
            unique_book = False
            book = Book.objects.get(pk=book_id)
            if 'isbn' in query:
                unique_book = Book.objects.filter(isbn__iexact=query['isbn']).\
                    exclude(pk=book_id)
            if unique_book:
                return LibraryResponse.response(
                    'error',
                    'This book already exists'
                )
            else:
                book_authors = []
                for author_id in query['authors'].split(','):
                    author = Author.objects.filter(pk=author_id)
                    if author:
                        book_authors.append(Author.objects.get(pk=author_id))
                    else:
                        return LibraryResponse.response(
                            'error',
                            'Author #%s not found' % author_id
                        )
                for field, value in query.items():
                    if field != 'authors':
                        setattr(book, field, value)
                book.save()
                if book_authors:
                    book.authors.clear()
                    for author in book_authors:
                        book.authors.add(author)
                return LibraryResponse.response(
                    'success',
                    'The book has been successfully updated'
                )
        except Book.DoesNotExist:
            return LibraryResponse.record_not_found()


def delete(r, book_id):
    try:
        book = Book.objects.get(pk=book_id)
        book.delete()
        return LibraryResponse.response(
            'success',
            'The book has been successfully deleted'
        )
    except Book.DoesNotExist:
        return LibraryResponse.record_not_found()


def other(r, book_id):
    raise Http404()