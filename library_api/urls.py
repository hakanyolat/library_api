from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^author/$', views.authors_view),
    url(r'^author/(?P<author_id>\d+)/$', views.author_detail_view),
    url(r'^book/$', views.books_view),
    url(r'^book/(?P<book_id>\d+)/$', views.book_detail_view),
    url(r'^library/$', views.library_view),
]
