from django.http import Http404
from library_api.models import Author, Book
from library_http import LibraryRequest, LibraryResponse


def get(r):
    try:
        return LibraryResponse.custom_response(Book.to_dict())
    except Book.DoesNotExist:
        return LibraryResponse.custom_response({})


def post(r):
    params = ['isbn', 'title', 'lc_classification', 'authors']
    if LibraryRequest(r).is_not_available(params):
        return LibraryResponse.invalid_request()
    elif not Book.fields_is_valid({k: r.POST[k] for k in params}):
        return LibraryResponse.invalid_request_parameters()
    else:
        try:
            book = Book.objects.get(isbn__iexact=r.POST['isbn'])
            return LibraryResponse.response(
                'error',
                'This book already exists'
            )
        except Book.DoesNotExist:
            new_book = Book(
                isbn=r.POST['isbn'],
                title=r.POST['title'],
                lc_classification=r.POST['lc_classification']
            )
            book_authors = []
            for author_id in r.POST['authors'].split(','):
                author = Author.objects.filter(pk=author_id)
                if author:
                    book_authors.append(Author.objects.get(pk=author_id))
                else:
                    return LibraryResponse.response(
                        'error',
                        'Author #%s not found' % author_id
                    )
            new_book.save()
            for author in book_authors:
                new_book.authors.add(author)
            return LibraryResponse.response(
                'success',
                'The book has been successfully added'
            )


def other(r):
    raise Http404()
