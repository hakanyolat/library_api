from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from library_http import LibraryRouter
import authors_view_methods
import author_detail_view_methods
import books_view_methods
import book_detail_view_methods
import library_view_methods


def index(request):
    return HttpResponse('Library api v1.0')


@csrf_exempt
def library_view(r):
    return LibraryRouter(r, library_view_methods).apply()


@csrf_exempt
def books_view(r):
    return LibraryRouter(r, books_view_methods).apply()


@csrf_exempt
def book_detail_view(r, book_id):
    return LibraryRouter(r, book_detail_view_methods).apply(book_id=book_id)


@csrf_exempt
def authors_view(r):
    return LibraryRouter(r, authors_view_methods).apply()


@csrf_exempt
def author_detail_view(r, author_id):
    return LibraryRouter(r, author_detail_view_methods).apply(
        author_id=author_id
    )
