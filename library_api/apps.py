from __future__ import unicode_literals

from django.apps import AppConfig


class LibraryApiConfig(AppConfig):
    name = 'library_api'
