from datetime import datetime
from django.http import Http404
from library_api.models import Author
from library_http import LibraryRequest, LibraryResponse


def get(r):
    try:
        return LibraryResponse.custom_response(Author.to_dict())
    except Author.DoesNotExist:
        return LibraryResponse.custom_response({})


def post(r):
    params = ['name', 'surname', 'date_of_birth']
    if LibraryRequest(r).is_not_available(params):
        return LibraryResponse.invalid_request()
    else:
        if not Author.fields_is_valid({k: r.POST[k] for k in params}):
            return LibraryResponse.invalid_request_parameters()
        try:
            author = Author.objects.get(surname__iexact=r.POST['surname'])
            return LibraryResponse.response(
                'error',
                'This author already exists'
            )
        except Author.DoesNotExist:
            new_author = Author(
                name=r.POST['name'],
                surname=r.POST['surname'],
                date_of_birth=datetime.strptime(
                    r.POST['date_of_birth'], '%d.%m.%Y'
                )
            )
            new_author.save()
            return LibraryResponse.response(
                'success',
                'The author has been successfully added'
            )


def other(r):
    raise Http404()
