from datetime import datetime
from django.http import Http404, QueryDict
from library_api.models import Author
from library_http import LibraryResponse


def get(r, author_id):
    try:
        return LibraryResponse.custom_response(Author.to_dict(
            multi=False, pk=author_id
        ))
    except Author.DoesNotExist:
        return LibraryResponse.record_not_found()


def put(r, author_id):
    return patch(r, author_id)


def patch(r, author_id):
    query = QueryDict(r.body).copy()
    params = ['name', 'surname', 'date_of_birth']
    params_map = [query.get(p, False) for p in params]
    requests_is_not_available = True in [
        False in params_map and r.method == 'PUT',
        all(i is False for i in params_map) and r.method == 'PATCH'
    ]
    if requests_is_not_available:
        return LibraryResponse.invalid_request()
    elif not Author.fields_is_valid(query):
        return LibraryResponse.invalid_request_parameters()
    else:
        try:
            author = Author.objects.get(pk=author_id)
            if author:
                unique_author = False
                if 'surname' in query:
                    unique_author = Author.objects.\
                        filter(surname__iexact=query['surname']).\
                        exclude(id=author_id)
                if 'date_of_birth' in query:
                    query['date_of_birth'] = datetime.strptime(
                        query['date_of_birth'],
                        '%d.%m.%Y'
                    )
                if unique_author:
                    return LibraryResponse.response(
                        'error',
                        'This author already exists'
                    )
                else:
                    for field, value in query.items():
                        setattr(author, field, value)
                    author.save()
                    return LibraryResponse.response(
                        'success',
                        'The author has been successfully updated'
                    )
        except Author.DoesNotExist:
            return LibraryResponse.record_not_found()


def delete(r, author_id):
    try:
        author = Author.objects.get(pk=author_id)
        author.delete()
        return LibraryResponse.response(
            'success',
            'The author has been successfully deleted'
        )
    except Author.DoesNotExist:
        return LibraryResponse.record_not_found()


def other(r, author_id):
    raise Http404()