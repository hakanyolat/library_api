from django.http import HttpResponse, Http404
from library_api.models import Author, Book
import json


class LibraryResponse(object):
    @staticmethod
    def response(response_type, response_message):
        return HttpResponse(json.dumps({
            'response': response_type,
            'message': response_message
        }), content_type='application/json')

    @classmethod
    def invalid_request(cls):
        return cls.response('error', 'Request is not available')

    @classmethod
    def invalid_request_parameters(cls):
        return cls.response('error', 'Invalid request parameters')

    @classmethod
    def record_not_found(cls):
        return cls.response('error', 'Record not found')

    @classmethod
    def invalid_csv_file(cls):
        return cls.response('error', 'Your csv file already contains an '
                                     'existing record or the file format '
                                     'is incorrect')

    @staticmethod
    def custom_response(response):
        return HttpResponse(
            json.dumps(response),
            content_type='application/json'
        )


class LibraryRequest(object):
    def __init__(self, request):
        self.r = request

    def is_not_available(self, params):
        return False in [self.r.POST.get(p, False) for p in params]

    @staticmethod
    def csv_file_is_not_available(csv_file):
        cols = [
            'isbn',
            'title',
            'lc classification',
            'name',
            'surname',
            'date of birth'
        ]
        cols = cols + cols[3:] + cols[3:]
        csv_cols = csv_file.readline().lower().rstrip().split(';')
        if cols != csv_cols or sum(1 for _ in csv_file) <= 1:
            return True
        else:
            for line in csv_file:
                if '' in line.split(';')[:3] or '' in line.split(';')[3:6]:
                    return True
        return False

    @staticmethod
    def get_library_from_csv_file(csv_file):
        books = []
        for line_number, line in enumerate(csv_file):
            if line_number:
                line_list = line.rstrip().split(';')
                book = line_list[:3]
                author_scope = line_list[3:]
                authors = []
                for i in xrange(0, 9, 3):
                    broken = '' in [author_scope[i+x] for x in xrange(0, 3)]
                    if not broken:
                        authors.append({
                            'name': author_scope[i+0],
                            'surname': author_scope[i+1],
                            'date_of_birth': author_scope[i+2]
                        })
                books.append({
                    'isbn': book[0],
                    'title': book[1],
                    'lc_classification': book[2],
                    'authors': authors
                })
        return books

    @staticmethod
    def library_is_not_available(method, library):
        isbns = []
        surnames = []
        for book in library:
            if not Book.fields_is_valid({
                'isbn': book['isbn'],
                'title': book['title'],
                'lc_classification': book['lc_classification'],
                'authors': '0'
            }) or book['isbn'] in isbns:
                return True
            else:
                isbns.append(book['isbn'])
                same_line_surnames = []
                for author in book['authors']:
                    if not Author.fields_is_valid({
                        'name': author['name'],
                        'surname': author['surname'],
                        'date_of_birth': author['date_of_birth']
                    }) or (method == 'PATCH' and
                            author['surname'] in same_line_surnames):
                        return True
                    else:
                        surnames.append(author['surname'])
                        same_line_surnames.append(author['surname'])
        if method == 'PATCH':
            for isbn in isbns:
                try:
                    same_book = Book.objects.get(isbn__iexact=isbn)
                    return True
                except Book.DoesNotExist:
                    pass
            for surname in surnames:
                try:
                    same_author = Author.objects.get(surname__iexact=surname)
                    return True
                except Author.DoesNotExist:
                    pass
        return False


class LibraryRouter(object):
    def __init__(self, request, module):
        self.r = request
        self.module = module
        self.method = self.r.method.lower() \
            if hasattr(self.module, self.r.method.lower()) \
            else 'other'

    def apply(self, **kwargs):
        return getattr(self.module, self.method)(r=self.r, **kwargs)
