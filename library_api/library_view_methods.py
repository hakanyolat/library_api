from datetime import datetime
from django.http import Http404, HttpResponse
from library_api.models import Author, Book
from library_http import LibraryRequest, LibraryResponse
from django.core.files.base import ContentFile


def post(r):
    csv = None
    if r.method == 'POST':
        csv = r.FILES['file'] if 'file' in r.FILES else None
    elif r.method == 'PATCH':
        csv = ContentFile(r.body) if len(r.body) else None
    else:
        return other(r)
    if not csv:
        return LibraryResponse.invalid_request()
    elif LibraryRequest.csv_file_is_not_available(csv):
        return LibraryResponse.invalid_csv_file()
    else:
        library = LibraryRequest.get_library_from_csv_file(csv)
        if LibraryRequest.library_is_not_available(r.method, library):
            return LibraryResponse.invalid_csv_file()
        else:
            if r.method == 'POST':
                Author.objects.all().delete()
                Book.objects.all().delete()
            for book in library:
                book_authors = []
                for author in book['authors']:
                    try:
                        exists_author = Author.objects.get(
                            surname__iexact=author['surname']
                        )
                        book_authors.append(exists_author)
                    except Author.DoesNotExist:
                        new_author = Author(
                            name=author['name'],
                            surname=author['surname'],
                            date_of_birth=datetime.strptime(
                                author['date_of_birth'], '%d.%m.%Y'
                            )
                        )
                        new_author.save()
                        book_authors.append(new_author)
                new_book = Book(
                    isbn=book['isbn'],
                    title=book['title'],
                    lc_classification=book['lc_classification'],
                )
                new_book.save()
                for author in book_authors:
                    new_book.authors.add(author)
            return LibraryResponse.response(
                'success',
                'The library has been successfully updated using the csv file'
            )


def patch(r):
    return post(r)


def other(r):
    raise Http404()
